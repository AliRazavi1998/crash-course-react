import React, { useState } from 'react'
import useInput from './Components/input'

function App() {

    const { fields, handelChange } = useInput({
        email: '',
        password: ''
    });
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-8 m-auto">
                    <div className="card">
                        <div className="card-header">Register</div>
                        <div className="card-body">
                            <div className="form-group mt-3">
                                <input className="form-control" name="email" type="email" value={fields.email} onChange={handelChange} />
                            </div>
                            <div className="form-group mt-3">
                                <input className="form-control" name="password" type="password" value={fields.password} onChange={handelChange} />
                            </div>
                            <div className="form-group mt-3">
                                <button className="btn btn-primary btn-sm">Register</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default App
