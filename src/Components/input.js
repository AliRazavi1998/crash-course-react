import React, { useState } from 'react'

function useInput(init) {
    const [fields, setFields] = useState(init);

    const handelChange = (event) => {
        setFields({
            ...fields,
            [event.target.name]: event.target.value
        });
    }

    return { fields, handelChange }
}

export default useInput
